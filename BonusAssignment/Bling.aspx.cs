﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class Bling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void PalindromeCheck(object sender, EventArgs e)
        {
            int halflength;
            string check = palindrome.Text.ToLower();
            string str1, str2;
            string noSpace=check.Replace(" ", string.Empty);
           
            halflength = (noSpace.Length) / 2;

            if ((noSpace.Length)%2==0)
            {
                str1 = noSpace.Substring(0, halflength);
            }
            else
            {
                str1 = noSpace.Substring(0, halflength+1);
            }
            str2 = noSpace.Substring(halflength);

            char[] charArray = str2.ToCharArray();
            Array.Reverse(charArray);
            String reversedString = new string(charArray);

            if (str1 == reversedString)
            {
                palidromeCheck.InnerHtml = "This is a palindrome!";
            } else
            {
                palidromeCheck.InnerHtml = "This is NOT palindrome!";
            }
        }
    }
}