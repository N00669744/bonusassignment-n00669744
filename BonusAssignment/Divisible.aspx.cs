﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class Divisible : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void PrimeNumber(object sender, EventArgs e)
        {
            int integer = int.Parse(integerInput.Text);
            int i = 0, zeroCount = 0;
            int prime;
            string primeMsg = "This is a Prime number";
            string notPrime = "This is NOT a Prime number";

            while ((integer-i)>1)
            {
                i++;
                prime = integer % (integer - i);
              
                if(prime == 0)
                {
                    zeroCount++;
                }
            }
            if (zeroCount>2)
            {
                primeOutput.InnerHtml = notPrime;
            }else
            {
                primeOutput.InnerHtml = primeMsg;
            }
            
        }
    }
}