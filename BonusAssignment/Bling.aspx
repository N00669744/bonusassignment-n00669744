﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bling.aspx.cs" Inherits="BonusAssignment.Bling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" for="palindrome">Palindrome Check: </asp:Label>
            <asp:TextBox runat="server" ID="palindrome"></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ControlToValidate="palindrome" ValidationExpression="^[a-zA-Z_ ]+$" ErrorMessage="Enter a Valid String!"></asp:RegularExpressionValidator>
        </div>
        
        <div>
            <asp:Button runat="server" OnClick="PalindromeCheck" Text="Submit" />
        </div>

        <div id="palidromeCheck" runat="server">

        </div>
    </form>
</body>
</html>
