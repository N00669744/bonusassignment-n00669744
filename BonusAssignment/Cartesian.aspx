﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="BonusAssignment.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Cartesian Smartesian</h2>
            <div>
                <label runat="server" for="xinput">X-Coordinate:</label>
                <asp:TextBox runat="server" id="xinput" placeholder="Enter X-Coordinate"></asp:TextBox>
                <asp:CompareValidator ControlToValidate="xinput" runat="server" Operator="NotEqual" ValueToCompare="0" 
                    ErrorMessage="Input cannot be 0" Type="Integer"></asp:CompareValidator>
            </div>

            <div>
                <label runat="server" for="yinput">Y-Coordinate:</label>
                <asp:TextBox runat="server" ID="yinput" placeholder="Enter Y-Coordinate"></asp:TextBox>
                 <asp:CompareValidator ControlToValidate="yinput" runat="server" Operator="NotEqual" ValueToCompare="0" 
                    ErrorMessage="Input cannot be 0" Type="Integer"></asp:CompareValidator>
            </div>

            <asp:Button runat="server" ID="submit" Text="Submit" OnClick="QuadrantCheck"/>

            <div id="quadrantDisplay" runat="server">
               

            </div>
        </div>
    </form>
</body>
</html>
