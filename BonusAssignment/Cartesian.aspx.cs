﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void QuadrantCheck(object sender, EventArgs e)
        {
            string outmsg = "Coordinate is in Quadrant ";
            int xcoordinate = int.Parse(xinput.Text);
            int ycoordinate = int.Parse(yinput.Text);

            if(xcoordinate>0 && ycoordinate>0)
            {
                outmsg += 1;
            }else if(xcoordinate < 0 && ycoordinate > 0)
            {
                outmsg += 2;
            } else if (xcoordinate < 0 && ycoordinate < 0)
            {
                outmsg += 3;
            }else
            {
                outmsg += 4;
            }
            quadrantDisplay.InnerHtml=outmsg;
        }
    }
}