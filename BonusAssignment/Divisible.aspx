﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Divisible.aspx.cs" Inherits="BonusAssignment.Divisible" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>Divisible Sizzable</h2>
            <label runat="server" for="integerInput">Integer Value: </label>
            <asp:TextBox runat="server" ID="integerInput"></asp:TextBox>
            <asp:CompareValidator runat="server" ControlToValidate="integerInput" 
                ValueToCompare="0" Operator="GreaterThan" ErrorMessage="Integers need to be greater than 0!">
            </asp:CompareValidator>
        </div>
        <div>
            <asp:Button runat="server" ID="Submit" Text="Submit" onclick="PrimeNumber" />
        </div>

        <div id="primeOutput" runat="server">

        </div>
    </form>
</body>
</html>
